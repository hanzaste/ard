//YWROBOT
//Compatible with the Arduino IDE 1.0
//Library version:1.1
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>

enum MENU {
  HOME_FIRST, HOME_SECOND, HOME_THIRD, W_POT, W_TEMP, W_LIGHT
  };
LiquidCrystal_I2C lcd(0x27,16,2);  // set the LCD address to 0x27 for a 16 chars and 2 line display

#define DOWN  4
#define UP    2
#define ENTER 3
#define BACK  5

byte buttonFlag = 0;

bool buttonEvent(int button)
{
  switch(button)
  {
    case UP:
     if (digitalRead(UP) == LOW)
     {
       buttonFlag |= 1;
     } else if (buttonFlag & 1)
     {
       buttonFlag ^= 1;
       return true;
     }
     break;

    case DOWN:
     if (digitalRead(DOWN) == LOW)
     {
       buttonFlag |= 2;
     } else if (buttonFlag & 2)
     {
       buttonFlag ^= 2;
       return true;
     }
     break;

    case BACK:
     if (digitalRead(BACK) == LOW)
     {
       buttonFlag |= 4;
     } else if (buttonFlag & 4)
     {
       buttonFlag ^= 4;
       return true;
     }
     break;

    case ENTER:
     if (digitalRead(ENTER) == LOW)
     {
       buttonFlag |= 8;
     } else if (buttonFlag & 8)
     {
       buttonFlag ^= 8;
       return true;
     }
  }
   return false;
}

void setup()
{
  lcd.init();                      // initialize the lcd 
  lcd.init();
  // Print a message to the LCD.
  lcd.backlight();
  pinMode(2, INPUT_PULLUP);
  pinMode(3, INPUT_PULLUP);
  pinMode(4, INPUT_PULLUP);
  pinMode(5, INPUT_PULLUP);
  Serial.begin(9600);
  printMenu(">Potentiometer", "Temperature");
}

MENU menu_state = HOME_FIRST;
void printMenu(char first[], char second[])
{
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print(first);
  lcd.setCursor(0,1);
  lcd.print(second);
  return;
}

void printMenu(char first[], double second)
{
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print(first);
  lcd.setCursor(0,1);
  lcd.print(second);
  return;
}

void printMenu(char first[], int second)
{
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print(first);
  lcd.setCursor(0,1);
  lcd.print(second);
  return;
}

void loop()
{
  int valPot = map(analogRead(1), 0, 1023, 0, 100);
  int valTemp = analogRead(2);//*4 - 2732;
  double valTemp2 = map(valTemp, 0, 1023, 0, 500) - 273.15;
  int valLight = map(analogRead(3), 15, 810, 100, 0);
  Serial.print("Pot: "); Serial.println(valPot);
  Serial.print("Temp: "); Serial.println(valTemp2);
  Serial.print("Light: "); Serial.println(valLight);
  
  switch (menu_state)
  {
    case HOME_FIRST:
      if (buttonEvent(UP)) {menu_state = HOME_THIRD; printMenu(">Light", "Potentiometer");}
      if (buttonEvent(DOWN)) {menu_state = HOME_SECOND; printMenu(">Temperature", "Light");}
      if (buttonEvent(ENTER)) {menu_state = W_POT; printMenu("Potentiometer:", valPot);}
      break;
    case HOME_SECOND:
      if (buttonEvent(UP)) {menu_state = HOME_FIRST; printMenu(">Potentiometer", "Temperature");}
      if (buttonEvent(DOWN)) {menu_state = HOME_THIRD; printMenu(">Light", "Potentiometer");}
      if (buttonEvent(ENTER)) {menu_state = W_TEMP; printMenu("Temperature:", valTemp2);}
      break;
    case HOME_THIRD:
      if (buttonEvent(UP)) {menu_state = HOME_SECOND; printMenu(">Temperature", "Light");}
      if (buttonEvent(DOWN)) {menu_state = HOME_FIRST; printMenu(">Potentiometer", "Temperature");}
      if (buttonEvent(ENTER)) {menu_state = W_LIGHT; printMenu("Light:", valLight);}
      break;
    case W_POT:
      lcd.setCursor(0,1);
      lcd.print(valPot);lcd.print("     ");
      if (buttonEvent(BACK)) {menu_state = HOME_FIRST; printMenu(">Potentiometer", "Temperature");}
      break;
    case W_TEMP:
      lcd.setCursor(0,1);
      lcd.print(valTemp2);lcd.print("     ");
      if (buttonEvent(BACK)) {menu_state = HOME_SECOND; printMenu(">Temperature", "Light");}
      break;
    case W_LIGHT:
      lcd.setCursor(0,1);
      lcd.print(valLight);lcd.print("     ");
      if (buttonEvent(BACK)) {menu_state = HOME_THIRD; printMenu(">Light", "Potentiometer");}
      break;
  };
}