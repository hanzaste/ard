char* letters[]={
  ".-", "-...", "-.-.", "-..", ".", "..-.", "--.",  // A-G
  "...." , "..", ".---", "-.-", ".-..", "--", "-.", // H-N
  "---", ".--.", "--.-", ".-.", "...", "-", "..-",  // O-U
  "...-", ".--", "-..-", "-.--", "--.."             // V-Z
};
char incomingChar = 0;

void turnLED(int value) {
  digitalWrite(9, value);
  digitalWrite(10, value);
  digitalWrite(11, value);
}

void flash(char string[], int len) {
  for (int i = 0; i<len; i++) {
    turnLED(1);
    if (string[i] == '.') {delay(200);}
    else if (string[i] == '-') {delay(800);}
    turnLED(0);
    delay(200);
  }
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
  Serial.println("Booting...");
}

void loop() {
  // put your main code here, to run repeatedly:
  bool validChar = true;
  int available = Serial.available();
  while(available > 0) {
    Serial.println("Got output.");
    incomingChar = Serial.read();
    //char validation
    if (incomingChar > 64 && incomingChar < 91) {incomingChar += 32;}
    if (incomingChar == ' ') {delay(1600); validChar = false;}
    if (!(incomingChar > 64 && incomingChar < 91 || incomingChar == ' ' || incomingChar > 95 && incomingChar < 122)) {validChar = false;}
    if (validChar) {
      Serial.print("Signaling: ");
      Serial.println(incomingChar);
      flash(letters[incomingChar - 97], strlen(letters[incomingChar - 97]));
    }
    available--;
  }
}