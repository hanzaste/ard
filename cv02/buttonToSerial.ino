enum states {
  STATE_NULL, STATE_SW1, STATE_SW2, STATE_SW1SW2
};

enum states state = STATE_NULL;
bool isSW1 = false; bool isSW2 = false;

void flash(int pin) {
  digitalWrite(pin, 1);
  delay(100);
  digitalWrite(pin, 0);
}

void setup() {
  Serial.begin(9600);
  pinMode(2, INPUT_PULLUP);
  pinMode(3, INPUT_PULLUP);
  pinMode(4, INPUT_PULLUP);
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
  Serial.println("Booting...");
}

void loop() {
  isSW1 = !digitalRead(3);
  isSW2 = !digitalRead(4);

  if (isSW1 && state == STATE_NULL) {state = STATE_SW1; Serial.println("State set to SW1.");}
  else if (isSW2 && state == STATE_NULL) {state = STATE_SW2; Serial.println("State set to SW2.");}

  if (isSW1 && state == STATE_SW2) {state = STATE_SW1SW2; Serial.println("State set to SW1SW2.");}
  else if (isSW2 && state == STATE_SW1) {state = STATE_SW1SW2; Serial.println("State set to SW1SW2.");}
  
  if (!isSW1 && !isSW2 && state != STATE_NULL) {
    if (state == STATE_SW1) {flash(11); Serial.println("Akce A.");}
    if (state == STATE_SW2) {flash(10); Serial.println("Akce B.");}
    if (state == STATE_SW1SW2) {flash(9); Serial.println("Akce C.");}

    state = STATE_NULL;
  }
}