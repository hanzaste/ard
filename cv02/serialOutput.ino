bool blue = false;
bool green = false;
bool red = false;
  
void setup() {
  // put your setup code here, to run once:
  //
  Serial.begin(9600);
  pinMode(2, INPUT_PULLUP);
  pinMode(3, INPUT_PULLUP);
  pinMode(4, INPUT_PULLUP);
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  if(!digitalRead(2)) {
    digitalWrite(9, 1);
    if (!blue) {Serial.println("Blue.");}
    blue = true;
  } else {
    digitalWrite(9, 0);
    blue = false;
  }
  if(!digitalRead(3)) {
    digitalWrite(10, 1);
    if (!green) {Serial.println("Green.");}
    green = true;
  } else {
    digitalWrite(10, 0);
    green = false;
  }
  if(!digitalRead(4)) {
    if (!red) {Serial.println("Red.");}
    red = true;    
    digitalWrite(11, 1);
  } else {
    digitalWrite(11, 0);
  }
}