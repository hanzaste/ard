#define RED 13              // číslo pinu, na kterém je červená LED
#define GREEN 12            // číslo pinu, na kterém je zelená LED
#define BLUE 11             // číslo pinu, na kterém je modrá LED

#define DOT 200             // doba bliknutí tečky
#define DASH 800            // doba bliknutí čárky
#define NO_LIGHT 200        // doba vypnutí LED mezi jednotlivými bliknutími
int color = 13;
int flag = 0;

void flash(int isSpace, int duration) {
  int isReleased = 0;
  double start = millis();
  if (isSpace) {
    while (millis() - start < duration) {
      isReleased = digitalRead(3);
      if (!isReleased && !flag) {
        changeColor();
        flag = 1;
      }
      if (isReleased) {
        flag = 0;
      }
    }
    return;
  }
  digitalWrite(color, 1);
  while (millis() - start < duration) {
    isReleased = digitalRead(3);
    if (!isReleased && !flag) {
      changeColor();
      flag = 1;
    }
    if (isReleased) {
      flag = 0;
    }
  }
  digitalWrite(color, 0);
}

void flashChange() {
  digitalWrite(color, 1);
  if (color == 13) {
    digitalWrite(11, 0);
    return;
  }
  digitalWrite(color + 1, 0);
}

void changeColor() {
  if (color == 11) {
    color = 13;
  }
  else {
    color--;
  }
  flashChange();
  return;
}

void setup() {
  // put your setup code here, to run once:
  pinMode(3, INPUT_PULLUP);
  pinMode(RED, OUTPUT);
  pinMode(GREEN, OUTPUT);
  pinMode(BLUE, OUTPUT);
}

void loop() {
  int button = digitalRead(3);
  //Letter A
  flash(0, DOT);
  flash(0, DASH);
  //Space
  flash(1, NO_LIGHT);
  //Letter R
  flash(0, DOT);
  flash(0, DASH);
  flash(0, DOT);
  //Space
  flash(1, NO_LIGHT);
  //Letter D
  flash(0, DASH);
  flash(0, DOT);
  flash(0, DOT);
  flash(1, 1000);
}