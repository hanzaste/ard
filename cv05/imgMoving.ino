#include <Esplora.h>
#include <SPI.h>
#include <SD.h>
#include <TFT.h>
#include <EEPROM.h>

#define SD_CS    8 

PImage logo;
int x; int y;

void setup() {
  // put your setup code here, to run once:
  EsploraTFT.begin();
  EsploraTFT.background(0, 0, 0);

  x = EEPROM.read(0);
  y = EEPROM.read(1);
  
  EsploraTFT.stroke(0, 0, 255);
  EsploraTFT.println();
  //EsploraTFT.println(F("Files:"));
  EsploraTFT.stroke(255, 255, 255);
  Serial.begin(9600);

  // try to access the SD card. If that fails (e.g.
  // no card present), the Esplora's LED will turn red.
  Serial.print(F("Initializing SD card..."));
  if (!SD.begin(SD_CS)) {
    Serial.println(F("No SD card inserted!"));
    return;
  }

  logo = EsploraTFT.loadImage("bi-ard.bmp");
    if (logo.isValid()) {
    Esplora.writeGreen(255);
  } else {
    EsploraTFT.println(F("bi-ard.bmp is missing!"));
    Esplora.writeRed(255);
    return;
  }

    EsploraTFT.background(0, 0, 0);
    EsploraTFT.image(logo, x, y);  
}

void loop() {
  int xJ = map(Esplora.readJoystickX(), 512, -512, 0, EsploraTFT.width() - logo.width());
  int yJ = map(Esplora.readJoystickY(), -512, 512, 0, EsploraTFT.height()  - logo.height());

  if (!Esplora.readButton(1))
  {
    EEPROM.update(0, x);
    EEPROM.update(1, y);
  }
  
  if (!Esplora.readJoystickSwitch())
  {
    x = xJ; y = yJ;
    EsploraTFT.background(0, 0, 0);
    EsploraTFT.image(logo, xJ, yJ);
  }
}