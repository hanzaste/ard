#include <Esplora.h>
#include <SPI.h>
#include <SD.h>
#include <TFT.h>
#include <EEPROM.h>

#define SD_CS    8 

int counter = 0;
int code[4];
int tryCode[4];
bool isPressed = false;
bool isUnlocked = false;

void setup() {
  // put your setup code here, to run once:
  EsploraTFT.begin();
  EsploraTFT.background(0, 0, 0);
  
  EsploraTFT.stroke(255, 255, 255);
  EsploraTFT.text("Wrong secret code!", 0, 0);
  EsploraTFT.text("*** Locked! ***", 0, 15);
  EsploraTFT.text("Enter code: ", 0, 30);
  Serial.begin(9600);
  

  // try to access the SD card. If that fails (e.g.
  // no card present), the Esplora's LED will turn red.
  Serial.print(F("Initializing SD card..."));
  if (!SD.begin(SD_CS)) {
    Serial.println(F("No SD card inserted!"));
    return;
  }

  File codeFile = SD.open("/code.txt");
  for (int i = 0; i < 4; i++) {code[i] = codeFile.read();}
}

void readCode(int buttonNum) {
  tryCode[counter] = buttonNum;
  EsploraTFT.text("*", 65 + counter*10, 30);
  counter++;
  isPressed = true;
  return;
}

bool validateCode() {
  for (int i = 0; i < 4; i++) {if (code[i] - '0' != tryCode[i]) {return false;}}
  return true;
}

bool getAnswer() {
  bool valid = validateCode();
  EsploraTFT.background(0, 0, 0);
  if (valid)
  {
  EsploraTFT.text("*** Unlocked! ***", 0, 0);
  }
  else
  {
  EsploraTFT.text("Wrong secret code!", 0, 0);
  EsploraTFT.text("*** Locked! ***", 0, 15);
  EsploraTFT.text("Enter code: ", 0, 30);
  }
  return valid;
}

void loop() {
  if (Esplora.readButton(1) && Esplora.readButton(2) && Esplora.readButton(3) && Esplora.readButton(4))
  {
    isPressed = false;
  }
  if (!isPressed && !isUnlocked)
  {
    if (!Esplora.readButton(1)) {readCode(1);}
    else if (!Esplora.readButton(2)) {readCode(2);}
    else if (!Esplora.readButton(3)) {readCode(3);}
    else if (!Esplora.readButton(4)) {readCode(4);}
  }
  if (counter == 4)
  {
  if (getAnswer() && !isUnlocked) {Esplora.writeGreen(255);    isUnlocked = true; return;}
  else {counter = 0;}
  }
}