#include <Esplora.h>
#include <SPI.h>
#include <SD.h>
#include <TFT.h>
#include <EEPROM.h>

#define SD_CS    8 

void setup() {
  // put your setup code here, to run once:
  EsploraTFT.begin();
  EsploraTFT.background(0, 0, 0);
  
  EsploraTFT.stroke(0, 0, 255);
  EsploraTFT.println();
  EsploraTFT.println(F("Files:"));
  EsploraTFT.stroke(255, 255, 255);
  Serial.begin(9600);

  // try to access the SD card. If that fails (e.g.
  // no card present), the Esplora's LED will turn red.
  Serial.print(F("Initializing SD card..."));
  if (!SD.begin(SD_CS)) {
    Serial.println(F("No SD card inserted!"));
    return;
  }

File root = SD.open("/");
  EsploraTFT.println(root.name());
  LoadDir (&root, 1);
}

void LoadDir (File* dir, int depth) {
  File entry;
  while (1)
  {
    entry = (*dir).openNextFile();
    if (!entry) {return;}
    for (int i = 0; i < depth; i++) {EsploraTFT.print("\t");}
    if (entry.isDirectory()) {EsploraTFT.println(entry.name()); LoadDir(&entry, depth + 1);}
    else {EsploraTFT.println(entry.name());}
  }
  entry.close();
}

void loop() {
  // put your main code here, to run repeatedly:

}