void setup() {
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  int x = analogRead(0);
  int y = analogRead(1);

  x = map(x, 0, 1023, -255, 255);
  y = map(y, 0, 1023, -255, 255);
  if (x > -7 && x < 7) x = 0;
  if (y > -7 && y < 7) y = 0;
  Serial.println(x);
  Serial.println(y);
  Serial.println("----");
  if(x >= 0) analogWrite(10, x);
  else analogWrite(10, -x);
  
  if(y >= 0) analogWrite(9, y);
  else analogWrite(11, -y);
}