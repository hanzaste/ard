void setup() {
  pinMode(5, OUTPUT);
  pinMode(8, INPUT_PULLUP);
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  int x = analogRead(0);
  int y = analogRead(1);
  int intensity = analogRead(2)/4;

  if (!digitalRead(8)) tone(5, intensity);
  else noTone(5);

  x = map(x, 0, 1023, -255, 255);
  y = map(y, 0, 1023, -255, 255);
  if (x > -7 && x < 7) x = 0;
  if (y > -7 && y < 7) y = 0;
  Serial.println(x);
  Serial.println(y);
  Serial.println("----");
  
  
  if (x == 0) analogWrite(10, 0);
  else {
  if(x > 0) analogWrite(10, intensity);
  else analogWrite(10, intensity);
  }

  if (y == 0) {analogWrite(9, 0); analogWrite(11, 0);}
  else {
  if(y > 0) analogWrite(9, intensity);
  else analogWrite(11, intensity);
  }
}