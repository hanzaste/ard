void setup() {
  pinMode(5, OUTPUT);
  pinMode(7, INPUT_PULLUP);
  pinMode(4, INPUT_PULLUP);
  pinMode(3, INPUT_PULLUP);
  pinMode(2, INPUT_PULLUP);
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
  Serial.begin(9600);
}

int score = 0;
int played = 0;

void loop()
{
  int num = (random(60000) % 4);             // je třeba, aby barvy blikaly náhodně
  played++;

  switch(num)
  {
    case 0:
      digitalWrite(9, 1);
      push_button(7, "red");       // pokud se rozsvítí červená LED, musíte zmáčknout SWITCH 1
      digitalWrite(9, 0);
      break;

    case 1:
      digitalWrite(10, 1);
      push_button(4, "green");     // pokud se rozsvítí zelená LED, musíte zmáčknout SWITCH 2
      digitalWrite(10, 0);
      break;

     case 2:
      digitalWrite(11, 1);
      push_button(3, "blue");       // pokud se rozsvítí modrá LED, musíte zmáčknout SWITCH 3
      digitalWrite(11, 0);
      break;

     case 3:
      digitalWrite(9, 1);
      digitalWrite(10, 1);
      push_button(2, "yellow");     // pokud se rozsvítí žlutá LED, musíte zmáčknout SWITCH 4
      digitalWrite(9, 0);
      digitalWrite(10, 0);
  }

   if(played >= 30)
   {

    Serial.print("Your score is "); Serial.print(score); Serial.println(".");

    Serial.println("");
    if(score > 15)
    {
      Serial.println("--- Winner! ---");
    }
    else
    {
      Serial.println("--- Loser! ---");
    }
    played = 0;
    score = 0;
  }
 }

void push_button( int button, char color[])
{
    for(int i=0; i < 35000; i++)                    // velikost i určuje dobu rozsvícení LED
    {
      if(!digitalRead(button) && (digitalRead(7) + digitalRead(4) + digitalRead(3) + digitalRead(2) == 3))
      {
        score++;
        Serial.println(color);
        break;
      }
    }
}