#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <SD.h>

const int chipSelect = 10;
File leadFile;

LiquidCrystal_I2C lcd(0x27,16,2);  // set the LCD address to 0x27 for a 16 chars and 2 line display

byte arrow[8] = {B10000, B11000, B11100, B11110, B11110, B11100, B11000, B10000};
byte lion[8] = {B00100, B00010, B10100, B11101, B10110, B00110, B01001, B10001};
byte clearSeg[8] = {B00000, B00000, B00000, B00000, B00000, B00000, B00000, B00000};

byte cactusBig[8] = {B00000, B00000, B00110, B00111, B10110, B11110, B00110, B00110};
byte cactusSmall[8] = {B00000, B00000, B00000, B00100, B01100, B11101, B01111, B01100};

byte runnerOne[8] = {B00110, B00111, B01100, B01100, B10100, B00111, B00101, B01000};
byte runnerTwo[8] = {B00000, B00110, B00111, B01100, B01110, B10100, B01010, B00010};
byte bullet[8] = {B00000, B00000, B00111, B01101, B11111, B01101, B00111, B00000};
byte bulletAir[8] = {B00000, B00000, B01110, B00000, B01111, B00000, B01110, B00000};

char homeText[][30] = {"Play", "Leaderboard", "Credits"};

enum WINDOW {
  HPL, HLC, HCP, WC, WP, WPN
} state;

enum GAME {
  PD, PU, O, END
} stateGame;

#define DOWN  4
#define UP    2
#define ENTER 3
#define BACK  5
#define MAX_SCORE 1000

byte buttonFlag = 0;

int score = 0;
byte runnerState = 0;
int layout[16] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
char nickname[4] = "AAA";
char leadNicks[10][4] = {"000", "000", "000", "000", "000", "000", "000", "000", "000", "000"};
int leadScores[10];

bool buttonEvent(int button)
{
  switch(button)
  {
    case UP:
     if (digitalRead(UP) == LOW)
     {
       buttonFlag |= 1;
     } else if (buttonFlag & 1)
     {
       buttonFlag ^= 1;
       return true;
     }
     break;

    case DOWN:
     if (digitalRead(DOWN) == LOW)
     {
       buttonFlag |= 2;
     } else if (buttonFlag & 2)
     {
       buttonFlag ^= 2;
       return true;
     }
     break;

    case BACK:
     if (digitalRead(BACK) == LOW)
     {
       buttonFlag |= 4;
     } else if (buttonFlag & 4)
     {
       buttonFlag ^= 4;
       return true;
     }
     break;

    case ENTER:
     if (digitalRead(ENTER) == LOW)
     {
       buttonFlag |= 8;
     } else if (buttonFlag & 8)
     {
       buttonFlag ^= 8;
       return true;
     }
  }
   return false;
}

void setup()
{
  lcd.init();                      // initialize the lcd 
  lcd.init();                      // initialize the lcd 
  // Print a message to the LCD.
  lcd.backlight();

  lcd.createChar(0, clearSeg);
  lcd.createChar(1, cactusSmall);
  lcd.createChar(2, cactusBig);
  lcd.createChar(3, arrow);
  lcd.createChar(4, lion);
  lcd.createChar(5, runnerOne);
  lcd.createChar(6, runnerTwo);
  lcd.createChar(7, bullet);
  //lcd.createChar(8, bulletAir);

  if (!SD.begin(chipSelect)) {while (true);}
  
  state = HPL; printHome();
  stateGame = PD;
}

void printHome ()
{
  lcd.clear();
  switch (state)
  {
    case HPL:
      lcd.setCursor(0,0); lcd.write(3); lcd.print(" "); lcd.print(homeText[0]);
      lcd.setCursor(0,1); lcd.print(homeText[1]);
    break;

    case HLC:
      lcd.setCursor(0,0); lcd.write(3); lcd.print(" "); lcd.print(homeText[1]);
      lcd.setCursor(0,1); lcd.print(homeText[2]);
    break;

    case HCP:
      lcd.setCursor(0,0); lcd.write(3); lcd.print(" "); lcd.print(homeText[2]);
      lcd.setCursor(0,1); lcd.print(homeText[0]);
    break;
  }
}

void printCredits ()
{
  lcd.clear();
  lcd.setCursor(0,0); lcd.print("by hanzaste");
  lcd.setCursor(0,1); lcd.print("FIT CTU "); lcd.write(4); lcd.print(" BI-ARD");
}

void printAllGameChars ()
{
  lcd.clear();
  lcd.setCursor(0,0); for (int i = 1; i < 7; i++) {lcd.write(i);}
}

void shiftLayout ()
{
  for (int i = 0; i < 15; i++) {layout[i] = layout[i + 1];}
  layout[15] = random(0, 8); layout[15] = layout[15] <= 5 ? 0 : layout[15] - 5;
  lcd.setCursor(0, 1);
  for (int i = 0; i < 16; i++) {lcd.write(layout[i]);}
  if (score >= MAX_SCORE) {stateGame = O;}
  lcd.setCursor(15 - (int)(log((float)score)/log(10)), 0); lcd.print(score);
  score++;
}

void wait () {for (int i = 0; i < 5; i++){delay(100); if (digitalRead(UP) == LOW) {stateGame = PU;}}}

void printScore ()
{
  lcd.clear();
  lcd.setCursor(2,0); lcd.write(5); lcd.print("GAME OVER."); lcd.write(6);
  lcd.setCursor((8 - (int)(log((float)score)/log(10)))/2, 1); lcd.print("SCORE: "); lcd.print(score);
  lcd.setCursor(15, 1); lcd.write(3);
}
void printIt (int it, int dir)
{
  lcd.setCursor(9 + it + dir*1, 0); lcd.write(' ');
  lcd.setCursor(9 + it, 0); lcd.print('V');
}

void printNick ()
{
  lcd.setCursor(3, 1); lcd.print("Nick: ");
  for (int i = 0; i < 3; i++) {lcd.write(nickname[i]);}
}

void nickSelect()
{
  bool arrowFlag = false;
  int nickIt = 0;
  for (int i = 0; i < 3; i++) {nickname[i] = 'A';}
  printIt(0, 0); printNick();
  while (nickIt != 3)
  {
    if (buttonEvent(ENTER)) {++nickIt; printIt(nickIt, -1);}
    else if (buttonEvent(BACK) && nickIt > 0) {--nickIt; printIt(nickIt, 1);}
    else if (buttonEvent(UP))
    {
      if (nickname[nickIt] == 'Z') {nickname[nickIt] = 'A';}
      else {nickname[nickIt]++;}
      printNick();
    }
    else if (buttonEvent(DOWN))
    {
      if (nickname[nickIt] == 'A') {nickname[nickIt] = 'Z';}
      else {nickname[nickIt]--;}
      printNick();
    }
    if (nickIt == 2) {lcd.setCursor(15, 1); lcd.write(3); arrowFlag = true;}
    else if (nickIt != 2 && arrowFlag == true) {lcd.setCursor(15, 1); lcd.write(' '); arrowFlag = false;}
  }
}

void playGame ()
{
  lcd.clear();
  //Play the game
  while (stateGame != END)
  {
    switch (stateGame)
    {
      case PD:
        shiftLayout();
        lcd.setCursor(1,1); lcd.write(5 + runnerState); runnerState = !runnerState;
        if (layout[1] != 0) {stateGame = O;}
        wait();
        break;
      case PU:
        for (int i = 0; i < 2; i++) {
          shiftLayout();
          lcd.setCursor(1,0); lcd.write(5 + runnerState); runnerState = !runnerState;
          wait();
        }
        lcd.setCursor(1,0); lcd.write(' ');
        stateGame = PD;
        break;
      case O:
        printScore();
        stateGame = END;
        break;
    }
  }

  while (state == WP)
  {
    if (buttonEvent(ENTER)) {state = WPN;}
    else if (buttonEvent(BACK)) {state = HPL;}
  }
  
  lcd.clear();
  if (state == WPN) {nickSelect(); state = HPL;}
  
  printHome();
  
  //Save score if top 10
  if (score > leadScores[9])
  {
    leaderboardUpdate();
  }
  
  //Reset the playing variables
  stateGame = PD;
  score = 0;
  runnerState = 0;
  for (int i = 0; i < 16; i++) {layout[i] = 0;}
}

void loadLeaderboardFromFile ()
{
  leadFile = SD.open("a.txt");
    
  char currChar;

  for (int i = 0; i < 10; i++)
  {
    //Load nick
    for (int j = 0; j < 3; j++)
    {
      leadNicks[i][j] = leadFile.read();
    }

    leadFile.read();
    
    //Load score
    leadScores[i] = leadFile.parseInt();

    leadFile.read();
  }

  leadFile.close();
}

void saveLeaderboardToFile ()
{
  SD.remove("a.txt");
  leadFile = SD.open("a.txt", FILE_WRITE);

  for (int i = 0; i < 10; i++)
  {
    for (int j = 0; j < 3; j++) {leadFile.write(leadNicks[i][j]);}

    leadFile.write(' ');

    leadFile.print(String(leadScores[i]));

    leadFile.write(' ');    
  }
  leadFile.close();
}

int DeleteIfUnique (char nickname[], int score)
{
    int i = 0;
    for (; i < 10; i++)
    {
        if (strcmp(leadNicks[i], nickname) == 0)
        {
            if (leadScores[i] < score)
            {
                for (int j = i + 1; j < 10; j++)
                {
                    leadNicks[j-1][0] = leadNicks[j][0];
                    leadNicks[j-1][1] = leadNicks[j][1];
                    leadNicks[j-1][2] = leadNicks[j][2];
                    leadScores[j-1] = leadScores[j];
                }
                leadScores[9] = -1;
                return 1;
            }
            else {return 0;}
        }
    }
    return 1;
}

void sortNicksSameScore ()
{
    int it = 0;

    while (it < 10)
    {
        int sameCount = 0;
        for (; it < 10; it++)
        {
            sameCount++;
            if (leadScores[it-1] != leadScores[it]) {break;}
        }
        if (sameCount > 1) {qsort(leadNicks[it-sameCount], sameCount, sizeof(*leadNicks), (int (*)(const void *, const void *))strcmp);}
        else {it++;}
    }
}

void leaderboardUpdate ()
{
    if (!DeleteIfUnique(nickname, score)) {return;}
    for (int i = 8; i >= 0; i--)
    {
        if (score > leadScores[i])
        {
            leadNicks[i+1][0] = leadNicks[i][0]; leadNicks[i+1][1] = leadNicks[i][1]; leadNicks[i+1][2] = leadNicks[i][2];
            leadScores[i+1] = leadScores[i];
            if (i == 0)
            {
                leadNicks[i][0] = nickname[0]; leadNicks[i][1] = nickname[1]; leadNicks[i][2] = nickname[2];
                leadScores[i] = score;
                break;
            }
        }
        else
        {
            if (leadScores[i] == score) {
                leadNicks[i + 1][0] = nickname[0];
                leadNicks[i + 1][1] = nickname[1];
                leadNicks[i + 1][2] = nickname[2];
                leadScores[i + 1] = score;
            }
            else
            {
                leadNicks[i + 1][0] = nickname[0];
                leadNicks[i + 1][1] = nickname[1];
                leadNicks[i + 1][2] = nickname[2];
                leadScores[i + 1] = score;
            }
            break;
        }
    }
    sortNicksSameScore();
    saveLeaderboardToFile();
}

void printLeaderboard (int startPlace)
{
  lcd.clear();
  
  for (int i = 0; i < 2; i++)
  {
    lcd.setCursor(0, i); lcd.print(i + startPlace + 1); lcd.print(' '); for (int j = 0; j < 3; j++)lcd.print(leadNicks[startPlace + i][j]);
    lcd.setCursor(15 - (int)(log((float)leadScores[startPlace + i])/log(10)), i); lcd.print(leadScores[startPlace + i]);
  }
}

void doLeaderboard ()
{
  loadLeaderboardFromFile();

  int startPlace = 0;

  printLeaderboard(startPlace);
  
  while (true)
  {
    if (buttonEvent(UP)) {startPlace != 0 ? startPlace-- : startPlace = startPlace; printLeaderboard(startPlace);}
    if (buttonEvent(DOWN)) {startPlace != 8 ? startPlace++ : startPlace = startPlace; printLeaderboard(startPlace);}
    if (buttonEvent(BACK)) {state = HLC; printHome(); break;}
  }
}

void loop()
{
  switch (state)
  {
    case HPL:
      if (buttonEvent(ENTER)) {state = WP; playGame();}
      if (buttonEvent(UP)) {state = HCP; printHome();}
      if (buttonEvent(DOWN)) {state = HLC; printHome();}
      break;
    case HLC:
      if (buttonEvent(ENTER)) {doLeaderboard();}
      if (buttonEvent(UP)) {state = HPL; printHome();}
      if (buttonEvent(DOWN)) {state = HCP; printHome();}
      break;
    case HCP:
      if (buttonEvent(ENTER)) {state = WC; printCredits();}
      if (buttonEvent(UP)) {state = HLC; printHome();}
      if (buttonEvent(DOWN)) {state = HPL; printHome();}
      break;
    case WC:
      if (buttonEvent(BACK)) {state = HCP; printHome();}
      break;
    case WP:
      if (buttonEvent(BACK)) {state = HPL; printHome();}
  };

}
