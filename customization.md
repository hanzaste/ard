<link rel="stylesheet" href="styles.css" />

# `Customization.cs`
This class defines one instance of customization. That implies, that you could have multiple customization types on one database
> However that is not proved, not sure if the idea is implemented further in the `Dbkit`...
## Workflow
**Firstly**, the constructor is called by the `DataSet.cs` class, that fills the `DataSet` and the custom info (the `CustomEnum` and `CustomType`) defined in `ModelSetup`. We can allow the custom behavior and define the `CustomType` in the `ModelSetup` class. The constructor is called by the `DataSet` class. 

The rest of the class is called manually (at least we think :D).
- **Firstly**, the `CheckDbItems()` method is called. The method checks the differences between the db and the dataset. This method will proceed its computation only the first time, behavior can be canceled with the `force` flag.
- **Secondly**, we call the `CheckCustomSetup()` method to validate the db for one `CustomType`(Customer) and one `CustomType` value (sometimes called *CustomItem*). The method validates that the item is valid and all tabs/columns that should be there for the *CustomItem* are there. Throws exception otherwise.

That is the basic workflow of this class. Note that some of the methods of this class are also used by other `Dbkit` methods.

## Methods

### `CheckCustomType<CustomType>()`
Checks if the `this.CustomEnum` is not null and if its of the type `CustomType`.
### `CheckCustomSetup<CustomType>()`
Checks the setup for specified custom value. If there are any mismatches between the db and dataset regarding the customs, the method throws an exception.
### `CheckDbItems()`
This method collects names of all of the database columns and compares them with the info from `this.DataSet.Tables`. The custom checking is regarding the custom instance of the `Customization` instance.

How it basically works:
```c#
// dataset is the defined structure of the db, while dbInfo is the real structure of the db.
foreach (tab in dataset.tables) {
    dtTab = dbInfo.Get(tab);
    if (!dbTab) {
        // If the tab is not in the db but is in the dataset and set to custom,
         // program  proceeds, throws an error: not found otherwise.
        if (tab.IsCustom) tab.IsInDb = false;
        else error();
    }
    else {
        if (dbSchemas.Length != datasetSchemas.Length) error();
        else {
            for (col in tab) {
                if (dbColumns.Contains(col)) {
                    // Checks if the number of appearances of the col in the db equals 
                     // the no. appearances in the dataset. Fe, if there is 5 col_1 in the
                     // db and only 4 col_1 in the dataset, method throws an error.
                    // Note: This is the best approximation of this part of the pseudo
                     // code. In the real code, datasetSchemaKeys is a backwards reference
                     // from tab. So for every tab, the number of its every col is
                     // validated throughout the entire db. We are not sure, why there is 
                     // so much redundancy.¯\_(ツ)_/¯
                    if (dbColumns[col].Count() != datasetSchemaKeys.Length) error();
                }
                else {
                    if (col.IsCustom) col.IsInDb = false;
                    else error();
                }
            }
        }
    }
}
```
### `CheckCustomItems<CustomType>()`
This method is important as it is used by the `CustomRequire()` and `CustomIgnore()` methods. Given an array of *CustomItems* it checks their validity. It also checks the two edge cases:
- `CustomItems.Empty()` - If we have f.e. `CustomRequire()` for none *CustomItems*,
- `CustomItems.Count() == this.CustomEnum.Count()` - If we have f.e. `CustomRequire()` for all *CustomItems*.

If any of the edge-cases above occur, the method will throw an exception.
<blockquote class="err">

Note that the `CustomItems.Count() == this.CustomEnum.Count()` is not a valid way to validate the case of all *CustomItems* present as it expects that *CustomItems* is distinct. **SO ALWAYS USE DISTINCT *CUSTOMITEMS* WHEN DECLARING CUSTOMS!!!**

</blockquote>

---
[**⬆** back to main page **⬆**](./index.md)